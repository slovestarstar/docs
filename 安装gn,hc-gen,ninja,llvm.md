- 打开Linux编译服务器终端。
- 安装gn
    1. 下载
        ```
        wget https://repo.huaweicloud.com/harmonyos/compiler/gn/1717/linux/gn-linux-x86-1717.tar.gz
        ```
    1. 解压
        ```
        mkdir ~/gn
        tar -xvf gn-linux-x86-1717.tar.gz -C ~/gn
        
        ```
    1. 设置:
        ```
        gedit ~/.bashrc
        export PATH=~/gn:$PATH
        保存退出;
        source ~/.bashrc
        ```
- 安装ninja
        ```
        wget https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.9.0/linux/ninja.1.9.0.tar
        tar -C ~/ -xvf ninja.1.9.0.tar
        gedit ~/.bashrc
        在文档末尾添加:
        export PATH=~/ninja:$PATH
        保存退出;
        source ~/.bashrc
        ```

- 安装hc-gen
    - hc-gen是鸿蒙驱动HDF依赖的工具 
        ```
        wget https://repo.huaweicloud.com/harmonyos/compiler/hc-gen/0.65/linux/hc-gen-0.65-linux.tar
        tar -C ~/ -xvf hc-gen-0.65-linux.tar
        gedit ~/.bashrc
        在文档末尾添加:
        export PATH=~/hc-gen:$PATH
        保存退出;
        source ~/.bashrc
        ```
- 安装LLVM
    ```
    wget https://repo.huaweicloud.com/harmonyos/compiler/clang/9.0.0-36191/linux/llvm-linux-9.0.0-36191.tar
    tar -C ~/ -xvf llvm-linux-9.0.0-36191.tar
    gedit ~/.bashrc
    在文档末尾添加:
    export PATH=~/llvm/bin:$PATH
    保存退出;
    source ~/.bashrc
        ```