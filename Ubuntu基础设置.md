# <h1>前提条件
- [Ubuntu基本设置](https://gitee.com/slovestarstar/docs/blob/master/Ubuntu%E5%9F%BA%E6%9C%AC%E8%AE%BE%E7%BD%AE.md)
# <h1>配置repo工具
- 如果你的Ubuntu系统上还没有配置repo命令,需要先下载repo进行配置;
 1. 安装curl
    ```
    mkdir ~/bin/
    sudo apt install curl 
    ```
1. 安装repo
    ```
    curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > ~/bin/repo
    ```
1. 给repo可执行的权限
    -可以将整个home目录设置为可读写,以免后面出错
    ```
    chmod +x ~/bin/repo 
    ``` 
1. 将repo所在目录添加到PATH环境变量中,示例安装在了~/bin/中
    - 将repo所在目录加到PATH环境变量中,并将这行代码追加到.bashrc文件中。
    ```
    echo 'export PATH=~/bin:$PATH' >> ~/.bashrc   
    ```
    - 在当前bash环境下读取并执行FileName中的命令
    ```
    source ~/.bashrc
    ```
1. 安装python
    ```
    sudo apt install python
    ```
1. 安装git
    ```
    sudo apt install git
    ```
# <h1>配置ssh公钥
    ```
    ssh-keygen -t rsa -C "你的gitee账号邮箱"
    cat ~/.ssh/id_rsa.pub
    ```
- 复制公钥,打开gitee仓库管理界面->部署公钥管理->添加部署公钥,添加生成的public key到仓库中.
    ```
    ssh -T git@gitee.com
    ```
- 首次使用需要确认并添加主机到本机SSH可信列表。

    若返回 Hi XXX! You've successfully authenticated, but Gitee.com does not provide shell access. 内容，则证明添加成功。