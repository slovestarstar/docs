- 去 nodejs [官网]( https://nodejs.org )看最新的版本号；比如现在看到的最新稳定版为：14.17.0 LTS;
- 如有必要，先安装curl:
```
curl：sudo apt install curl ;
```
- 在终端执行：
```
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - ;
```
- 执行：
```
sudo apt-get install -y nodejs ;
```
检查安装结果：
```
node -v ;
```