- 安装virtualenv
    ```
    pip3 install virtualenv
    sudo apt install virtualenv

    # 创建使用python3.8位默认python解释器的virtualenv
    mkdir ~/harmonyos/venv && virtualenv -p python3.8 ~/harmonyos/venv
    # 激活virtualenv,激活后的pip3 install 会将包文件缓存到相应的子目录中
    source ~/harmonyos/venv/bin/activate
    # 安装setuptools 和 kconfiglib
    pip3 install setuptools kconfiglib
    cd /home/xgh/harmonyos/venv/bin/
    python -m pip install --upgrade pip
    ```
- 安装并升级python包管理工具(pip3):
    ```
    sudo apt-get install python3-setuptools python3-pip -y
    sudo pip3 install --upgrade pip
    ```
- 可选:将激活脚本添加到bashrc中,下次登录默认自动激活此python虚拟环境,可以使用deactivate使虚拟环境无效
        ```
        gedit  ~/.bashrc
        添加到末尾
        source ~/harmonyos/venv/bin/activate
        保存退出
        ```