# <h1>deveco device tool 打开Ubuntu中的openharmony源代码

## <h2>前提条件

[windows开发环境准备](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/Windows%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E5%87%86%E5%A4%87.md#zh-cn_topic_0000001058091994_section71401018163318)

[windows10 安装Ubuntu虚拟机](https://gitee.com/slovestarstar/docs/blob/master/%E5%9C%A8windows10%E4%B8%AD%E5%AE%89%E8%A3%85Ubuntu%E8%99%9A%E6%8B%9F%E6%9C%BA.md)

[Ubuntu基本设置](https://gitee.com/slovestarstar/docs/blob/master/Ubuntu%E5%9F%BA%E6%9C%AC%E8%AE%BE%E7%BD%AE.md)

[Ubuntu基础设置](https://gitee.com/slovestarstar/docs/blob/master/Ubuntu%E5%9F%BA%E7%A1%80%E8%AE%BE%E7%BD%AE.md)

[Ubuntu中安装sshd服务](https://gitee.com/slovestarstar/docs/blob/master/%E5%9C%A8Ubuntu%E4%B8%AD%E5%AE%89%E8%A3%85sshd%E6%9C%8D%E5%8A%A1.md)

[Ubuntu中安装配置samba](https://gitee.com/slovestarstar/docs/blob/master/ubuntu%E4%B8%AD%E5%AE%89%E8%A3%85%E9%85%8D%E7%BD%AEsamba.md)

[下载openharmony1.1.0源代码](https://gitee.com/slovestarstar/docs/blob/master/%E4%B8%8B%E8%BD%BDopenharmony1.1.0%E6%BA%90%E7%A0%81.md)

## <h2>打开visual studio code

- 切换到deveco device tool
- 点击Open DevECO Project,找到网络驱动器openharmony源代码的存放位置,确定,再确定.





