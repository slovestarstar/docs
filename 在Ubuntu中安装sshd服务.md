# <h1>前提条件
[在Ubuntu安装配置samba](https://gitee.com/slovestarstar/docs/blob/master/ubuntu%E4%B8%AD%E5%AE%89%E8%A3%85%E9%85%8D%E7%BD%AEsamba.md)
# <h1>在Ubuntu中安装sshd服务
- 在linux中一般以"d"结尾的,都是一种服务.
- ssh:Secure Shell
- 服务端:sshd

- 如果想让宿主机通过ssh访问虚拟机,需要在Ubuntu中安装openssh-server;

  ```
  sudo apt-get install openssh-server
  ```

- 确认sshserver是否启动

  ```
  ps -e |grep ssh
  ```

- 如果看到sshd,说明ssh-server已经启动了.
- 如果没有下面命令启动:

```
	sudo /etc/init.d/ssh start
```

- ssh-server配置文件位于 "/etc/ssh/sshd_config",在这里可以定义ssh的服务端口,默认端口是22,可以自定义成其他端口号,然后重启ssh服务:

  ```
  sudo /etc/init.d/ssh stop
  sudo /etc/init.d/ssh start
  ```

- 在宿主机使用下面命令登录ssh:

  ```
  ssh xxx@192.168.x.xxx  虚拟机ip
  ```

- @前为Ubuntu的用户名

- 退出登录

  ```
  exit
  ```

  