# <h1>简介
HiSpark WiFi IoT套件基于华为海思3861芯片
- [3861开发板介绍](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/Hi3861%E5%BC%80%E5%8F%91%E6%9D%BF%E4%BB%8B%E7%BB%8D.md)
    - Hi3861V100 芯片内置**2MB** Flash
    - **2.4GHz WiFi**
# <h1>从零开始搭建HarmonyOS开发环境
## <h2>开发环境搭建
1. [Ubuntu基本环境官方文档](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/Ubuntu%E7%BC%96%E8%AF%91%E7%8E%AF%E5%A2%83%E5%87%86%E5%A4%87.md)
1. [windows基本环境官方文档](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/Windows%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E5%87%86%E5%A4%87.md)
1. [3861开发环境官方文档](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/%E5%AE%89%E8%A3%85%E5%BC%80%E5%8F%91%E6%9D%BF%E7%8E%AF%E5%A2%83.md)
### <h3>Linux主机:源码下载,编译环境
- 配置Ubuntu环境
    - 基础设置
        1. 关闭Ubuntu自动更新
        1. 更改更新源,选择阿里云或者华为云,都比较快
        1. 更改系统时区
        1. 关闭节能,blank screen 设置为 never
        1. 打开文件 ,设置整个home 目录为所有人可读写

    - 配置repo工具
        - 如果你的Ubuntu系统上还没有配置repo命令,需要先下载repo进行配置;
        1. 安装curl
            ```
            mkdir ~/bin/
            sudo apt install curl 
            ```
        1. 安装repo
            ```
            curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > ~/bin/repo
            ```
        1. 给repo可执行的权限
            -可以将整个home目录设置为可读写,以免后面出错
            ```
            chmod +x ~/bin/repo 
            ``` 
        1. 将repo所在目录添加到PATH环境变量中,示例安装在了~/bin/中
            - 将repo所在目录加到PATH环境变量中,并将这行代码追加到.bashrc文件中。
            ```
            echo 'export PATH=~/bin:$PATH' >> ~/.bashrc   
            ```
            - 在当前bash环境下读取并执行FileName中的命令
            ```
            source ~/.bashrc
            ```
        1. 安装python
            ```
            sudo apt install python
            ```
        1. 安装git
            ```
            sudo apt install git
            ```
- 下载HarmonyOS源码
    ```
    mkdir -p ~/harmonyos/openharmony && cd ~/harmonyos/openharmony
    git config --global user.name "slovestarstar"
    git config --global user.email "xgh771@126.com"
    git config --global credential.helper store 
    sudo apt install git-lfs
    repo init -u https://gitee.com/openharmony/manifest.git -b master --no-repo-verify
    repo sync -c
    ```
### <h3>Ubuntu编译构件环境
在Ubuntu主机上配置鸿蒙源代码的编译构建环境,所有命令均通过远程终端在Ubuntu主机上执行;

注意:官方文档和视频教程的内容不同,昨天按照视频教程下载工具,出现错误,今天按照官方文档操作.
[Hi3861安装开发板环境官方文档](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/%E5%AE%89%E8%A3%85%E5%BC%80%E5%8F%91%E6%9D%BF%E7%8E%AF%E5%A2%83.md#section7438245172514)
#### <h4>官方环境安装
##### <h5>安装Scons
1. 打开Linux编译服务器终端.
1. 安装更新python3
    - 如果低于python3.7版本，不建议直接升级，请按照如下步骤重新安装。以python3.8为例，按照以下步骤安装python。   
    1. 我的Ubuntu版本为18.04,其他版本请参考官方文档.
        ```
        配置pip包的安装源,加速国内安装pip包:
        mkdir ~/.pip/
        cat <<EOF >>  ~/.pip/pip.config
        [global]
        index-url = https://mirrors.huaweicloud.com/repository/pypi/simple
        trusted-host = mirrors.huaweicloud.com
        timeout = 120
        EOF

        sudo apt-get install python3.8 python3-pip
        ```
    1. 安装virtualenv
        ```
        用了virtualenv,你的系统里就可以有很多python开发环境
        pip3 install virtualenv
        sudo apt install virtualenv
        # 创建使用python3.8位默认python解释器的virtualenv
        mkdir ~/harmonyos/venv && virtualenv -p python3.8 ~/harmonyos/venv
        # 激活virtualenv,激活后的pip3 install 会将包文件缓存到相应的子目录中
        source ~/harmonyos/venv/bin/activate
        # 安装setuptools 和 kconfiglib
        pip3 install setuptools kconfiglib
        ```
    1. 安装编译Hi3861需要的pip包
        ```
        pip install scons ecdsa pycryptodome
        pip3 install --upgrade --ignore-install six
        ```
    1. 可选:将激活脚本添加到bashrc中,下次登录默认自动激活此python虚拟环境,可以使用deactivate使虚拟环境无效
        ```
        cat <<EOF>> ~/.bashrc
        source ~/harmonyos/venv/bin/activate
        EOF
        ```
1. 安装gn
    - 打开linux编译服务器终端
        ```
        wget https://repo.huaweicloud.com/harmonyos/compiler/gn/1717/linux/gn-linux-x86-1717.tar.gz
        tar -C ~/ -xvf gn-linux-x86-1717.tar.gz
        gedit ~/.bashrc
        在文档末尾添加:
        export PATH=~/gn:$PATH
        保存退出;
        source ~/.bashrc
        ```
        **出错的原因找到,官方更新了gn版本,所以这里还是跟着官方文档来配置,视频教程仅供参考流程.**
1. 安装ninja
    - 打开linux编译服务器终端
        ```
        wget https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.9.0/linux/ninja.1.9.0.tar
        tar -C ~/ -xvf ninja.1.9.0.tar
        gedit ~/.bashrc
        在文档末尾添加:
        export PATH=~/ninja:$PATH
        保存退出;
        source ~/.bashrc
        ```
1. 安装hc-gen
    - 打开linux编译服务器终端
        ```
        wget https://repo.huaweicloud.com/harmonyos/compiler/hc-gen/0.65/linux/hc-gen-0.65-linux.tar
        tar -C ~/ -xvf hc-gen-0.65-linux.tar
        gedit ~/.bashrc
        在文档末尾添加:
        export PATH=~/hc-gen:$PATH
        保存退出;
        source ~/.bashrc
        ```
1. 安装LLVM
    - 打开linux编译服务器终端
        ```
        wget https://repo.huaweicloud.com/harmonyos/compiler/clang/9.0.0-36191/linux/llvm-linux-9.0.0-36191.tar
        tar -C ~/ -xvf llvm-linux-9.0.0-36191.tar
        gedit ~/.bashrc
        在文档末尾添加:
        export PATH=~/llvm/bin:$PATH
        保存退出;
        source ~/.bashrc
        ```
1. 安装Scons
    ```
    python3 -m pip install scons
    ```
1. 安装python模块
    - 运行如下命令,安装python模块setuptools.
    ```
    pip3 install setuptools
    ```
1. 安装GUI menuconfig工具（Kconfiglib），建议安装Kconfiglib 13.2.0+版本。
    ```
    sudo pip3 install kconfiglib
    ```
1. 安装pycryptodome
    - 安装升级文件签名依赖的Python组件包，包括：pycryptodome、six、ecdsa。安装ecdsa依赖six，请先安装six，再安装ecdsa。
    ```
    sudo pip3 install pycryptodome
    ```
1. 安装six
    ```
    sudo pip3 install six --upgrade --ignore-installed six
    ```
1. 安装ecdsa 
    ```
    sudo pip3 install ecdsa
    ```

1. 安装gcc_riscv32(wlan模组类编译工具链)
    - 打开Linux编译服务器终端
        ```
        sudo apt-get install gcc && sudo apt-get install g++ && sudo apt-get install flex bison && sudo apt-get install texinfo
        ```
        wget https://repo.huaweicloud.com/harmonyos/compiler/gcc_riscv32/7.3.0/linux/gcc_riscv32-linux-7.3.0.tar.gz
    - 配置ssh公钥
        ```
        ssh-keygen -t rsa -C "xgh771@126.com"
        cat ~/.ssh/id_rsa.pub
        ```
        - 复制公钥,打开gitee仓库管理界面->部署公钥管理->添加部署公钥,添加生成的public key到仓库中.
        ```
        ssh -T git@gitee.com
        ```
        首次使用需要确认并添加主机到本机SSH可信列表。

        若返回 Hi XXX! You've successfully authenticated, but Gitee.com does not provide shell access. 内容，则证明添加成功。
    - 下载riscv-gnu-toolschain交叉编译工具链
        ```
        git clone --recursive https://gitee.com/mirrors/riscv-gnu-toolchain.git

        ```
        ---
    - 打开文件夹riscv-gnu-toolchain，先删除空文件夹，以防止下载newlib，binutils，gcc时冲突。
        ```
        cd riscv-gnu-toolchain && rm -rf riscv-newlib && rm -rf riscv-binutils && rm -rf riscv-gcc
        ```
    - 下载riscv-newlib-3.0.0
        ```
        git clone -b riscv-newlib-3.0.0 https://github.com/riscv/riscv-newlib.git
        ```
    - 下载riscv-binutils-2.31.1
        ```
        git clone -b riscv-binutils-2.31.1 https://github.com/riscv/riscv-binutils-gdb.git
        ```
    - 下载riscv-gcc-7.3.0。
        ```
        git clone -b riscv-gcc-7.3.0 https://github.com/riscv/riscv-gcc
        ```
    - 添加riscv-gcc-7.3.0补丁。
        ```
        wget https://repo.huaweicloud.com/harmonyos/compiler/gcc_riscv32/7.3.0/linux/gcc_riscv32-linux-7.3.0.tar.gz
        ```
1. 安装hb
    - 安装方法
        ```
        
        python3.8 -m pip install --user ohos-build
        gedit ~/.bashrc
        在文档末尾添加:
        export PATH=~/.local/bin:$PATH
        保存退出;
        source ~/.bashrc

        ```
# <h1>编译源码

#### <h4>安装文件系统打包工具
1. 运行"mkfs.vfat",如果未找到该命令,需要安装;
1. 运行"mcopy",如果未找到该命令,需要安装
    ```
    sudo apt-get install dosfstools mtools
    sudo apt-get install zip
    ```
#### <h4>下载,配置编译工具链
使用如下命令,分别下载gn,ninja,llvm,hc-gen包,根据官方文档修改,一步到位,不用反复复制粘贴
gn/ninja/clang是鸿蒙系统源码构件框架依赖的工具
hc-gen是鸿蒙驱动框架相关工具
gcc_riscv32是3861平台需要的编译工具链
- 下载gn/ninja/LLVM/hc-gen包:

~~wget https://repo.huaweicloud.com/harmonyos/compiler/gn/1523/Linux/gn.1523.tar~~

~~wget https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.9.0/Linux/ninja.1.9.0.tar~~

~~wget https://repo.huaweicloud.com/harmonyos/compiler/clang/9.0.0-34042/Linux/llvm-linux-9.0.0-34042.tar~~

~~wget https://repo.huaweicloud.com/harmonyos/compiler/hc-gen/0.65-linux.tar~~

~~wget https://repo.huaweicloud.com/harmonyos/compiler/gcc_riscv32/7.3.0/linux/gcc_riscv32-linux-7.3.0.tar.gz~~

- 解压gn/ninja/llvm/hc-gen包
```
tar -C ~/ -xvf gn.1523.tar
tar -C ~/ -xvf ninja.1.9.0.tar
tar -C ~/ -xvf llvm-linux-9.0.0-34042.tar
tar -C ~/ -xvf hc-gen-0.65-linux.tar
tar -C ~/ -xvf gcc_riscv32-linux-7.3.0.tar.gz
```
### <h3>DevEco Device Tools的安装配置
DevEco Device Tools是VS code的插件;
1. 下载安装 [VS code](https://code.visualstudio.com/)
1. 下载安装[node.js](https://nodejs.org/en/)
1. 下载安装[openJDK](https://mirror.tuna.tsinghua.edu.cn/AdoptOpenJDK/16/jdk/x64/windows/)
1. 安装hpm和其他npm包:
    - 使用国内镜像源,加速模块下载速度:
    - 安装hpm鸿蒙组件管理器;
    - 安装Windows编译工具宝,serialport包需要编译本地模块,必须先安装此包才能安装serialport包;
    - 安装tftp包,用于启动tftp服务器,通过网络方式向单板烧录映像;
    - 安装serialport包,用于通过串口烧写映像;
    ```
    npm config set registry https://registry.npm.taobao.org
    npm install -g @ohos/hpm-cli
    npm install -g windows-build-tools
    npm install -g tftp
    npm install -g serialport
    ```
1. 下载安装[DevEco Device Tools](https://device.harmonyos.com/cn/ide#download_release)
### <h3>Windows主机:代码编辑,二进制少些环境
## <h2>开发环境配置

## <h2>HarmonyOS源码下载

## <h2>HarmonyOS源码编译

## <h2>二进制文件烧写

# <h1>使用HarmonyOS控制的各个扩展板(IO设备,传感器,OLED等)

# <h1>使用HarmonyOS创建热点,连接其他热点

# <h1>使用HarmonyOS进行网络编程

# <h1>使用HarmonyOS进行物联网应用开发
