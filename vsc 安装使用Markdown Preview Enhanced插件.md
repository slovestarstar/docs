# <h1>概述:

gitee文档都是md格式,为了在本地编辑预览md文件,只需在visual studio code中安装Markdown Preview Enhanced插件即可;
# <h1>安装:
- 打开visual studio code ;
- 左侧点击extension ;
- 上不搜索框输入Markdown Preview Enhanced;
- 找到Markdown Preview Enhanced,点击install ;
# <h1>source control:
- 点击左侧source control 按钮 ;
- 输入仓库地址 ,开始同步仓库 ;
- 插件设置:file->preferences->settings,搜索Markdown-preview-enhanced;
# <h1>编写Markdown
## <h2>Markdown基本要素
### <h3>什么是Markdown?
Markdown 是一种文本格式。你可以用它来控制文档的显示。使用 markdown，你可以创建粗体的文字，斜体的文字，添加图片，并且创建列表 等等。基本上来讲，Markdown 就是普通的文字加上 # 或者 * 等符号。
### <h3>语法说明
#### <h4>标题
```
# 这是 <h1> 一级标题

## 这是 <h2> 二级标题

### 这是 <h3> 三级标题

#### 这是 <h4> 四级标题

##### 这是 <h5> 五级标题

###### 这是 <h6> 六级标题
```
如果你想要给你的标题添加 id 或者 class，请在标题最后添加 {#id .class1 .class2}。例如：
```
# 这个标题拥有 1 个 id {#my_id}

# 这个标题有 2 个 classes {.class1 .class2}
```
#### <h4>强调
```
*这会是 斜体 的文字*
_这会是 斜体 的文字_

**这会是 粗体 的文字**
__这会是 粗体 的文字__

_你也 **组合** 这些符号_

~~这个文字将会被横线删除~~
```
#### <h4>列表
##### <h5>无序列表
```
- Item 1
- Item 2
  - Item 2a
  - Item 2b
```
##### <h5>有序列表
```
1. Item 1
1. Item 2
1. Item 3
   1. Item 3a
   1. Item 3b
```
#<h5>添加图片
```
![GitHub Logo](/images/logo.png)
Format: ![Alt Text](url)
```
#<h5>链接
```
https://github.com - 自动生成！
[GitHub](https://github.com)
```
##### <h5>分割线
```
如下，三个或者更多的
---
连字符
***
星号
---

下划线
```
##### <h5>行内代码
```
我觉得你应该在这里使用
`<addr>` 才对。
```
##### <h5>代码块
你可以在你的代码上面和下面添加 三个"`"来表示代码块。
##### <h5>语法高亮
你可以给你的代码块添加任何一种语言的语法高亮
例如，给 ruby 代码添加语法高亮：(使用"````ruby"标记开始)

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```
##### <h5>代码块class(MPE扩展的特性)
你可以给你的代码块设置class.
例如,添加class1 class2 到一个代码块:
```
```javascript {.class1 .class}
function add(x, y) {
  return x + y
}
```

##### <h5> 代码行数
如果你想要你的代码显示代码行数,只要添加 line-numbers class 就可以了.
例如:
```
```javascript {.line-numbers}
function add(x, y) {
  return x + y
}
```
##### <h5>高亮代码行数
你可以通过添加highlight 属性的方式来高亮代码行数:
```
```javascript {highlight=10}

```javascript {highlight=10-20}

```javascript {highlight=[1-10,15,20-22]}
```
#### <h4>任务列表
```
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] 这是一个已经完成的列表
- [ ] 这是一个未完成的列表
```
#### <h4>表格
```
表头1 |表头2
------------ | -------------
单元格1内容 | 单元格2内容
第一列内容 | 第二列内容
```

