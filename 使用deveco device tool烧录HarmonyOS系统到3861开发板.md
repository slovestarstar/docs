# <h1>使用deveco device tool烧录HarmonyOS系统到3861开发板

## <h2> 前提条件

- [使用devEvo device tool编译 HarmonyOS源码](https://gitee.com/slovestarstar/docs/blob/master/%E4%BD%BF%E7%94%A8devEvo%20device%20tool%E7%BC%96%E8%AF%91%20HarmonyOS%E6%BA%90%E7%A0%81.md)

## <h2>开始烧录工作

- 连接好电脑和待烧录开发板，需要连接USB口
- 安装CH341SER.EXE,[下载地址](http://www.wch.cn/downloads/CH341SER_EXE.html),安装完可能需要重新插拔usb口
- 打开windows设备管理器,检查串口编号,下面以我的串口号COM4为例,在设备管理器中显示为USB-SERIAL CH340(COM4)
- 打开DevEco Device Tool，在Projects中，点击**Settings**打开工程配置界面
- common configuration 无需更改,deveco device tool已针对Hi3861系列开发板进行适配，无需单独修改
  - upload_port：COM4
  - upload_protocol：选择烧录协议，固定选择“burn-serial”。
  - upload_partitions：选择待烧录的文件，默认选择hi3861_app。
- 所有的配置都修改完成后，在工程配置页签的顶部，点击**Save**进行保存。
- 打开工程文件，在DevEco Device Tool界面的“PROJECT TASKS”中，点击hi3861下的**Upload**按钮，启动烧录。
  - 这里需要注意:需要把编译好的程序,拷贝到./out/wifiiot/目录.
- 启动烧录后，显示如下提示信息时，请按开发板上的RST按钮重启开发板。
  - 这里的注意事项:需要在界面提示按"reset"的几秒内按压reset,超时会失败,需要重新启动烧录.

