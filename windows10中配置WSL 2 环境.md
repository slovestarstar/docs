# <h1> 下载安装docker：
1. [docker下载地址](https://www.docker.com/products/docker-desktop) ；
1. 开始安装；
1. 下一步；
1. close and restart;
1. 重启后耐心等待弹出对话框：“WSL 2 installation is incomplete.
1. 点击对话框中的链接“https://aka.ms/wsl2kernel”，跟随网站指引继续；
# <h1>下载安装WSL2 Linux内核更新包；
1. 开始安装；
1. 重新启动；
1. 使用管理员身份打开powershell；
1. 运行下面的命令，将WSL 2设置为默认版本：
    ```
    wsl --set-default-version 2;
    ```
1. 继续刚才的网页，安装Ubuntu；
1. 安装windows终端；
1. wsl更改国内源：
```
    sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak
    sudo sed -i 's/security.ubuntu/mirrors.aliyun/g' /etc/apt/sources.list
    sudo sed -i 's/archive.ubuntu/mirrors.aliyun/g' /etc/apt/sources.list
    sudo apt update
    sudo apt-get upgrade
```
1. 在Ubuntu中安装gedit：sudo apt install gedit ;
