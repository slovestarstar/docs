# <h1>下载安装git
1. [下载git](https://git-scm.com/download/ )；
1. 安装git ；
1. 把git安装目录下的bin目录路径完整加入Path环境变量 ；
1. 在gitee.com用邮箱注册账号 ；
# <h1>配置ssh公钥
1. 生成git 配置ssh公钥,把公钥配置到码云：
    - 从git安装目录打开git-bash ;
    - ssh-keygen -t rsa -C "youremail" ;
    - 在windows资源管理器中打开用户目录下的.ssh目录 ；
    - 用记事本打开id_rsa.pub公钥文件，将其内容填入码云的该页面:  https://gitee.com/profile/sshkeys ，我的公钥里面，确定；
# <h1>初始化本地仓库
- 建立一个本地目录，作为本地仓库，进入这个目录;
- 在git-bash中执行下面的命令初始化，让git管理目录:
```
git init 
```
# <h1>配置用户信息：
```
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global credential.helper store ；
```
