1. 安装docker
    ```
    sudo apt install  docker.io
    ```
1. 下载openHarmonyOS docker
    ```
    sudo apt install apt-utils
    mkdir openharmony
    cd openharmony
    git clone https://gitee.com/openharmony/docs.git
    cd docs/docker
    sudo ./build.sh
    ```
- 出现"debconf: delaying package configuration, since apt-utils is not installed",不用管
    ```
    sudo docker run -it -v $(pwd):/home/openharmony openharmony-docker:0.0.4
    ```
- 把下载解压好的源码拷贝到openharmony目录
   ```
   hb set .
   ```
- 3861选择wifiiot_hispark_pegasus