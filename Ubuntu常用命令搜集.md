- 更改文件的用户组：
    ```
    sudo chown yourname file/dir name ;
    ```
- 如果想一同修改子文件夹：
    ```
    sudo chown yourname file/dir name/ -R ;
    ```
- source命令的作用
    - 在当前bash环境下读取并执行FileName中的命令。
    - 命令格式:
    ```
    source filename
    ```
- tar 注意参数的大小写含义是不一样的
    ```
    -C<目的目录>或--directory=<目的目录> 切换到指定的目录。
    tar -xvf etcbak.tar                 解开一个tar
    tar -cvf etcbak.tar etc/          打包一个tar
    tar -xvf  etcbak.tar  -C ./dir  解开一个tar到当前的 dir 目录中
    ```
