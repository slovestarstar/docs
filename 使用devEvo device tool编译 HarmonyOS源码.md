# <h1>使用devEvo device tool编译 HarmonyOS源码

## <h2> 前提条件

- [Ubuntu中安装sshd服务](https://gitee.com/slovestarstar/docs/blob/master/%E5%9C%A8Ubuntu%E4%B8%AD%E5%AE%89%E8%A3%85sshd%E6%9C%8D%E5%8A%A1.md)
- [deveco device tool打开Ubuntu中的HarmonyOS源码](https://gitee.com/slovestarstar/docs/blob/master/deveco%20device%20tool%20%E6%89%93%E5%BC%80Ubuntu%E4%B8%AD%E7%9A%84openharmony%E6%BA%90%E4%BB%A3%E7%A0%81.md)

## <h2> 开始编译工作

- 打开DevEco Device Tool工具，点击“View > Terminal”，进入终端界面。

- 在终端界面使用ssh命令连接linux服务器，如“ssh *user*@*ipaddr*”。

- 进入代码根路径，并在终端窗口，执行脚本命令
    ```
    hb set
    .
    ```
- 选择需要编译的版本“wifiiot_hispark_pegasus”。

- 执行
    ```
    hb build
    ```
    启动版本构建

- 构建成功后，会在./out/wifiiot/路径中生成以下文件，使用如下命令可以查看，至此编译构建流程结束。

  ```
  ls -l out/hispark_pegasus/wifiiot_hispark_pegasus/
  ```

  

