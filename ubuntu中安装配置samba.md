# <h1>虚拟机中安装配置samba
## <h2>windowsSMB协议:
    SMB:Sever Message Block
## <h2>安装
	    sudo apt install samba
	    sudo gedit /etc/samba/smb.conf

- 在文件末尾添加:

  ```
  [home]
  comment = user homes
  browserable = yes
  path = /home
  guest ok = yes
  read only = no
  writable = yes
  create mask = 0755
  ```
- 在workgroup后面加一行
    ```
    netbios name = ubt
    ```
- 保存退出

- 重启samba服务

  ```
  sudo service smbd restart
  ```

- 为samba增加用户

  ```
  sudo smbpasswd -a yourname
  ```

- 回到windows,映射网络驱动器

- 在文件管理器地址栏输入:

  ```
  \\ubt 虚拟机ip地址
  ```

- 右键点击出现的文件夹,选择映射网络驱动器