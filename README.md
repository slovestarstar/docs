# 鸿蒙开发小白文档

## <h2> 介绍
零基础学习开发鸿蒙的文档，尽量使初学者避坑

## <h2> 文档列表
1. [windows10中安装Ubuntu虚拟机](https://gitee.com/-/ide/project/slovestarstar/docs/edit/master/-/%E5%9C%A8windows10%E4%B8%AD%E5%AE%89%E8%A3%85Ubuntu%E8%99%9A%E6%8B%9F%E6%9C%BA.md)
1. [虚拟机网络配置](https://gitee.com/-/ide/project/slovestarstar/docs/edit/master/-/VMware%E8%99%9A%E6%8B%9F%E6%9C%BA%E4%B8%8B%E4%B8%89%E7%A7%8D%E7%BD%91%E7%BB%9C%E6%A8%A1%E5%BC%8F%E9%85%8D%E7%BD%AE.md)
1. [ubuntu基本设置](https://gitee.com/slovestarstar/docs/blob/master/Ubuntu%E5%9F%BA%E6%9C%AC%E8%AE%BE%E7%BD%AE.md)
1. [Ubuntu基础设置](https://gitee.com/-/ide/project/slovestarstar/docs/edit/master/-/Ubuntu%E5%9F%BA%E7%A1%80%E8%AE%BE%E7%BD%AE.md)
1. [Ubuntu中安装nodejs](https://gitee.com/slovestarstar/docs/blob/master/Ubuntu%E4%B8%AD%E5%AE%89%E8%A3%85nodejs.md)
1. [Ubuntu中安装gn,hc-gen,ninja,llvm](https://gitee.com/-/ide/project/slovestarstar/docs/edit/master/-/%E5%AE%89%E8%A3%85gn,hc-gen,ninja,llvm.md)

1. [git从零学](https://gitee.com/slovestarstar/docs/blob/master/git%E4%BB%8E%E9%9B%B6%E5%AD%A6.md)
1. [vsc 安装使用Markdown Preview Enhanced插件,md基本使用](https://gitee.com/slovestarstar/docs/blob/master/vsc%20%E5%AE%89%E8%A3%85%E4%BD%BF%E7%94%A8Markdown%20Preview%20Enhanced%E6%8F%92%E4%BB%B6.md)
1. [windows10中配置WSL 2 环境](https://gitee.com/slovestarstar/docs/blob/master/windows10%E4%B8%AD%E9%85%8D%E7%BD%AEWSL%202%20%E7%8E%AF%E5%A2%83.md)
1. [Ubuntu常用宏命令搜集](https://gitee.com/slovestarstar/docs/blob/master/Ubuntu%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4%E6%90%9C%E9%9B%86.md)
1. [win10中安装配置git](https://gitee.com/-/ide/project/slovestarstar/docs/edit/master/-/windows10%20%E4%B8%AD%E5%AE%89%E8%A3%85%E9%85%8D%E7%BD%AEgit.md)
1. [下载HarmonyOS1.1.0源代码](https://gitee.com/slovestarstar/docs/blob/master/%E4%B8%8B%E8%BD%BDopenharmony1.1.0%E6%BA%90%E7%A0%81.md)
1. [使用devEvo device tool编译 HarmonyOS源码](https://gitee.com/slovestarstar/docs/blob/master/%E4%BD%BF%E7%94%A8devEvo%20device%20tool%E7%BC%96%E8%AF%91%20HarmonyOS%E6%BA%90%E7%A0%81.md)
1. [使用deveco device tool烧录HarmonyOS系统到3861开发板](https://gitee.com/slovestarstar/docs/blob/master/%E4%BD%BF%E7%94%A8deveco%20device%20tool%E7%83%A7%E5%BD%95HarmonyOS%E7%B3%BB%E7%BB%9F%E5%88%B03861%E5%BC%80%E5%8F%91%E6%9D%BF.md)
## <h2> 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



## <h2> 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
