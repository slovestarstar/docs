# <h1>前提条件
- [在windows10中安装Ubuntu虚拟机](https://gitee.com/slovestarstar/docs/blob/master/%E5%9C%A8windows10%E4%B8%AD%E5%AE%89%E8%A3%85Ubuntu%E8%99%9A%E6%8B%9F%E6%9C%BA.md)
# <h1>更改Ubuntu显示分辨率：
- VMware中设置
    1. 关闭虚拟机:
    1. 在VMware中右键点击虚拟机
    1. 选择设置
    1. 显示器
    1. 指定监视器设置
    1. 任意监视器的最大分辨率
    1. 1920×1080
    1. 确定；
- 虚拟机设置:
    1. 运行虚拟机
    1. 进入设置
    1. device
    1. Display
    1. resolution
    1. 1920×1200
    1. apply
```
进入设置的方法：
    - 点击Ubuntu右上角的倒三角
    - 在弹出窗口中点击左下角的工具图标，进入设置；
```
# <h1>更改界面语言（可选）：
1. 设置-》region & language-
1. manage installed languages；
    - 如果弹出no language infomation available,点击update；
    - 如果弹出 the language support is not installed completely 点击install；
1. 点击install/remove languages
1. 选中Chinese（simplifled）
1. apply->language
1. 选择汉语
1. restart-logout
1. 重新登录，选择保留旧有的名称；(否则一些用户文档目录名会变为中文)
# <h1>关闭Ubuntu自动更新：
1. Application,打开方法为Ubuntu界面的左下角9个点;
1. software updates
1. update
1. automaticlly check for update
1. never Notify me or  a new ubuntu version
1. never；
# <h1>更改Ubuntu更新源：
1. Ubuntu software
1. download from
1. mirrors.huaweiclound.com
1. reload；
# <h1>更改时间：
1. 右上角点击 下三角图标
1. 点击工具图标
1. 左侧选择detail
1. date&time
1. time zone；
1. 在地图上选择中国；
