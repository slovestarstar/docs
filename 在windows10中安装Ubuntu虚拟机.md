# <h1>windows10中安装Ubuntu虚拟机
1. 在bios里打开cpu的虚拟化技术选项：找到Intel Virtualization Technology 选项，将其参数设置为enabled；
1. 进入windows10，安装vmware；[下载地址](https://my.vmware.com/en/web/vmware/downloads/info/slug/desktop_end_user_computing/vmware_workstation_player/16_0)
1. 在VMware中创建虚拟机；
1. 在虚拟机中安装Ubuntu18.04.5版本；[下载地址](https://releases.ubuntu.com/18.04.5/ubuntu-18.04.5-desktop-amd64.iso)
1. 设置虚拟机的用户名和密码；（密码请牢记，否则可能需要重装Ubuntu）
1. 设置虚拟机名称：Ubuntu_18.04.5_x64;
1. 将最大磁盘大小，设置为100g及以上；
1. 打开自定义硬件进行检查，处理器选项，确保打开虚拟化Intel-x/EPT 或 AMD-V/RVI(V)选项；
1. 网络适配器选择桥接模式(自动)；
1. 设置完成后，开始安装Ubuntu虚拟机，耐心等待完成。
