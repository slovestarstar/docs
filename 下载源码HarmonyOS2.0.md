# <h1>下载HarmonyOS2.0源码
- 准备目录
    ```
    mkdir -p ~/harmonyos/openharmony2.0 && cd ~/harmonyos/openharmony2.0
    ```
 - 使用镜像站点下载源码,然后解压,将源码拷贝到openharmony2.0这个目录
    ```
    wget https://repo.huaweicloud.com/harmonyos/os/2.0/code-2.0-canary.tar.gz
    ```
    