# <h1>安装和配置python
以下以Ubuntu18.04.5为基础,其他的版本轻参考官方文档;

1. 打开Linux编译服务器终端。
    - 配置pip包的安装源,加速国内安装pip包:
    ```
    mkdir ~/.pip/
    cd ~/.pip/
    touch pip.config
    gedit pip.config
    ```

    - 将下列内容考入文件,保存关闭:
    ```
    [global]
    index-url = https://mirrors.huaweicloud.com/repository/pypi/simple
    trusted-host = mirrors.huaweicloud.com
    timeout = 120
    EOF
    ```
1. 安装python3.8
    ```
    sudo apt-get install python3.8 
    ```
1. 安装pip3
    ```
    sudo apt-get install python3-pip
    ``` 