# <h1>使用HiBurn烧录Wi-Fi IoT设备
- 这个方法比使用deveco device tool 简单
1. 打开HiBurn
    - settings
    - 选择串口端口
    - 选择波特率115200
    - 选择文件
    - 找到编译好的二进制文件所在目录(Hi3861_wifiiot_app_allinone.bin)
    - 勾选自动烧录
    - 点击连接
1. 下面文本框显示connect的时候,按主板的rst
1. 显示success的时候,等30秒不动,点击disconnect,然后rst
